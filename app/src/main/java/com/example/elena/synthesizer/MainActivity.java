package com.example.elena.synthesizer;

import android.content.pm.ActivityInfo;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    Button
           first_do, first_re, first_mi, first_fa, first_sol, first_la, first_si, first_do_diez, first_re_diez, first_fa_diez, first_sol_diez, first_la_diez;

    private SoundPool soundPool;
    private int sound_first_do, sound_first_re, sound_first_mi, sound_first_fa, sound_first_sol, sound_first_la, sound_first_si,
            sound_first_do_diez, sound_first_re_diez, sound_first_fa_diez, sound_first_sol_diez, sound_first_la_diez;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE); //для альбомного режима


        first_do = findViewById(R.id.BtnDo_first_octave);
        first_re = findViewById(R.id.BtnRe_first_octave);
        first_mi = findViewById(R.id.BtnMi_first_octave);
        first_fa = findViewById(R.id.BtnFa_first_octave);
        first_sol = findViewById(R.id.BtnSol_first_octave);
        first_la = findViewById(R.id.BtnLa_first_octave);
        first_si = findViewById(R.id.BtnSi_first_octave);
        first_do_diez = findViewById(R.id.BtnDodiez_first_octave);
        first_re_diez = findViewById(R.id.BtnRediez_first_octave);
        first_fa_diez = findViewById(R.id.BtnFadiez_first_octave);
        first_sol_diez = findViewById(R.id.BtnSoldiez_first_octave);
        first_la_diez = findViewById(R.id.BtnLadiez_first_octave);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            soundPool = new SoundPool.Builder().setMaxStreams(5).build();
        } else {
            soundPool = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
        }

        sound_first_do = soundPool.load(this, R.raw.first_octave_do, 1);
        sound_first_re = soundPool.load(this, R.raw.first_octave_re, 1);
        sound_first_mi = soundPool.load(this, R.raw.first_octave_mi, 1);
        sound_first_fa = soundPool.load(this, R.raw.first_octave_fa, 1);
        sound_first_sol = soundPool.load(this, R.raw.first_octave_sol, 1);
        sound_first_la = soundPool.load(this, R.raw.first_octave_la, 1);
        sound_first_si = soundPool.load(this, R.raw.first_octave_si, 1);
        sound_first_do_diez = soundPool.load(this, R.raw.first_octave_do_diez, 1);
        sound_first_re_diez = soundPool.load(this, R.raw.first_octave_re_diez, 1);
        sound_first_fa_diez = soundPool.load(this, R.raw.first_octave_fa_diez, 1);
        sound_first_sol_diez = soundPool.load(this, R.raw.first_octave_sol_diez, 1);
        sound_first_la_diez = soundPool.load(this, R.raw.first_octave_la_diez, 1);

        // устанавливаем один обработчик для всех кнопок
        first_do.setOnClickListener(this);
        first_re.setOnClickListener(this);
        first_mi.setOnClickListener(this);
        first_fa.setOnClickListener(this);
        first_sol.setOnClickListener(this);
        first_la.setOnClickListener(this);
        first_si.setOnClickListener(this);
        first_do_diez.setOnClickListener(this);
        first_re_diez.setOnClickListener(this);
        first_fa_diez.setOnClickListener(this);
        first_sol_diez.setOnClickListener(this);
        first_la_diez.setOnClickListener(this);
    }
            @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.BtnDo_first_octave:
                    soundPool.play(sound_first_do, 1, 1, 0, 0, 1);
                    break;
                case R.id.BtnRe_first_octave:
                    soundPool.play(sound_first_re, 1, 1, 0, 0, 1);
                    break;
                case R.id.BtnMi_first_octave:
                    soundPool.play(sound_first_mi, 1, 1, 0, 0, 1);
                    break;
                case R.id.BtnFa_first_octave:
                    soundPool.play(sound_first_fa, 1, 1, 0, 0, 1);
                    break;
                case R.id.BtnSol_first_octave:
                    soundPool.play(sound_first_sol, 1, 1, 0, 0, 1);
                    break;
                case R.id.BtnLa_first_octave:
                    soundPool.play(sound_first_la, 1, 1, 0, 0, 1);
                    break;
                case R.id.BtnSi_first_octave:
                    soundPool.play(sound_first_si, 1, 1, 0, 0, 1);
                    break;
                case R.id.BtnDodiez_first_octave:
                    soundPool.play(sound_first_do_diez, 1, 1, 0, 0, 1);
                    break;
                case R.id.BtnRediez_first_octave:
                    soundPool.play(sound_first_re_diez, 1, 1, 0, 0, 1);
                    break;
                case R.id.BtnFadiez_first_octave:
                    soundPool.play(sound_first_fa_diez, 1, 1, 0, 0, 1);
                    break;
                case R.id.BtnSoldiez_first_octave:
                    soundPool.play(sound_first_sol_diez, 1, 1, 0, 0, 1);
                    break;
                case R.id.BtnLadiez_first_octave:
                    soundPool.play(sound_first_la_diez, 1, 1, 0, 0, 1);
                    break;
            }
    }
}